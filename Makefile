.PHONY := all

ROOT_URL := https://storage.googleapis.com/brain-genomics-public/research$\
	/proteins
PROTCNN_MODEL_URL := $(ROOT_URL)/pfam/models$\
	/single_domain_per_sequence_zipped_models/seed_random_32.0$\
	/5356760.tar.gz
PROTCNN_VOCAB_URL := $(ROOT_URL)/pfam/models$\
	/single_domain_per_sequence_zipped_models$\
	/trained_model_pfam_32.0_vocab.json
PROTNLM_MODEL_URL := $(ROOT_URL)/protnlm/uniprot_2022_04$\
	/savedmodel__20221011__030822_1128_bs1.bm10.eos_cpu/saved_model.pb
PROTNLM_VARS_IDX_URL := $(ROOT_URL)/protnlm/uniprot_2022_04$\
	/savedmodel__20221011__030822_1128_bs1.bm10.eos_cpu/variables$\
	/variables.index
PROTNLM_VARS_DATA_URL := $(ROOT_URL)/protnlm/uniprot_2022_04$\
	/savedmodel__20221011__030822_1128_bs1.bm10.eos_cpu/variables$\
	/variables.data-00000-of-00001

all: protcnn protnlm trained_model_pfam_32.0_vocab.json

clean:
	rm -rf protcnn protnlm

protcnn:
	wget -q $(PROTCNN_MODEL_URL)
	tar -zxvf 5356760.tar.gz
	mv trn-_cnn_random__random_sp_gpu-cnn_for_random_pfam-5356760 protcnn
	rm 5356760.tar.gz

protnlm:
	mkdir -p protnlm/variables
	wget -q $(PROTNLM_MODEL_URL) -P protnlm
	wget -q $(PROTNLM_VARS_IDX_URL) -P protnlm/variables
	wget -q $(PROTNLM_VARS_DATA_URL) -P protnlm/variables

trained_model_pfam_32.0_vocab.json:
	wget -q $(PROTCNN_VOCAB_URL)
