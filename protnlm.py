import re
import os
import sys

import numpy as np
import tensorflow as tf
import tensorflow_text

def query(seq):
    return f"[protein_name_in_english] <extra_id_0> [sequence] {seq}"

EC_NUMBER_REGEX = r'(\d+).([\d\-n]+).([\d\-n]+).([\d\-n]+)'

def run_inference(seq, imported, infer):
    labeling = infer(tf.constant([query(seq)]))
    names = labeling['output_0'][0].numpy().tolist()
    scores = labeling['output_1'][0].numpy().tolist()
    beam_size = len(names)
    names = [names[beam_size-1-i].decode().replace('<extra_id_0> ', '') for i in range(beam_size)]
    for i, name in enumerate(names):
        if re.match(EC_NUMBER_REGEX, name):
            names[i] = 'EC:' + name
        scores = [np.exp(scores[beam_size-1-i]) for i in range(beam_size)]
    return names, scores

def fasta_reader(path):
    last_id = ''
    last_seq = ''
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.strip()
            if line.startswith('>'):
                if last_id and last_seq:
                    yield (last_id, last_seq)
                last_id = line.lstrip('>').lstrip()
                last_seq = ''
            else:
                last_seq = last_seq + line
    if last_id and last_seq:
        yield (last_id, last_seq)

def main():
    in_fasta = sys.argv[1]

    script_dir = os.path.dirname(os.path.realpath(__file__))
    model_dir = os.path.join(script_dir, 'protnlm')

    imported = tf.saved_model.load(export_dir="protnlm")
    infer = imported.signatures["serving_default"]

    for record in fasta_reader(in_fasta):
        sequence = record[1].rstrip('*')
        names, scores = run_inference(sequence, imported, infer)
        for name, score, i in zip(names, scores, range(len(names))):
              print(f"{record[0]} ({i+1}): [{name}] with score {score:.03f}")

if __name__ == '__main__':
    main()
