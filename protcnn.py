import json
import os
import sys

import numpy as np
import tensorflow.compat.v1 as tf

AMINO_ACID_VOCABULARY = [
    'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R',
    'S', 'T', 'V', 'W', 'Y'
]

def residues_to_one_hot(amino_acid_residues):
    """
    Given a sequence of amino acids, return one hot array.
    Supports ambiguous amino acid characters B, Z, and X by distributing evenly
    over possible values, e.g. an 'X' gets mapped to [.05, .05, ... , .05].

    Supports rare amino acids by appropriately substituting. See
    normalize_sequence_to_blosum_characters for more information.

    Supports gaps and pads with the '.' and '-' characters; which are mapped to
    the zero vector.

    Parameters
    ----------
    amino_acid_residues : string
        Amino acid sequence with residues from AMINO_ACID_VOCABULARY.

    Returns
    -------
    A numpy array of shape (len(amino_acid_residues), len(AMINO_ACID_VOCABULARY)).

    Raises
    ------
    ValueError:
        if amino_acid_residues has a character not in the vocabulary +
        X.
    """
    to_return = []
    normalized_residues = amino_acid_residues.replace('U', 'C').replace('O', 'X')
    for char in normalized_residues:
        if char in AMINO_ACID_VOCABULARY:
            to_append = np.zeros(len(AMINO_ACID_VOCABULARY))
            to_append[AMINO_ACID_VOCABULARY.index(char)] = 1.
            to_return.append(to_append)
        elif char == 'B':  # Asparagine or aspartic acid.
            to_append = np.zeros(len(AMINO_ACID_VOCABULARY))
            to_append[AMINO_ACID_VOCABULARY.index('D')] = .5
            to_append[AMINO_ACID_VOCABULARY.index('N')] = .5
            to_return.append(to_append)
        elif char == 'Z':  # Glutamine or glutamic acid.
            to_append = np.zeros(len(AMINO_ACID_VOCABULARY))
            to_append[AMINO_ACID_VOCABULARY.index('E')] = .5
            to_append[AMINO_ACID_VOCABULARY.index('Q')] = .5
            to_return.append(to_append)
        elif char == 'X':
            to_return.append(
                np.full(len(AMINO_ACID_VOCABULARY), 1. / len(AMINO_ACID_VOCABULARY))
            )
        elif char == '.' or char == '-':
            to_return.append(np.zeros(len(AMINO_ACID_VOCABULARY)))
        else:
            raise ValueError('Could not one-hot code character {}'.format(char))
    return np.array(to_return)

def pad_one_hot_sequence(sequence: np.ndarray, target_length) -> np.ndarray:
    """
    Pads one hot sequence [seq_len, num_aas] in the seq_len dimension.

    Parameters
    ----------
    sequence : np.ndarray
        Encoded sequence.
    target_length : int
        Length to pad the sequences.

    Returns
    -------
    A numpy array padded to target_length.

    Raises
    ------
    ValueError:
        If a negative pad is requested.
    """
    sequence_length = sequence.shape[0]
    pad_length = target_length - sequence_length
    if pad_length < 0:
        raise ValueError(
            'Cannot set a negative amount of padding. Sequence length was {}, target_length was {}.'
            .format(sequence_length, target_length)
        )
    pad_values = [[0, pad_length], [0, 0]]

    return np.pad(sequence, pad_values, mode='constant')

def fasta_reader(path):
    last_id = ''
    last_seq = ''
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.strip()
            if line.startswith('>'):
                if last_id and last_seq:
                    yield (last_id, last_seq)
                last_id = line.lstrip('>').lstrip()
                last_seq = ''
            else:
                last_seq = last_seq + line
    if last_id and last_seq:
        yield (last_id, last_seq)

def main():
    in_fasta = sys.argv[1]

    script_dir = os.path.dirname(os.path.realpath(__file__))
    model_path = os.path.join(script_dir, 'protcnn')
    pfam_accs_path = os.path.join(
        script_dir, 'trained_model_pfam_32.0_vocab.json'
    )

    sess = tf.Session()
    graph = tf.Graph()
    predictions = []
    with graph.as_default():
        saved_model = tf.saved_model.load(sess, ['serve'], model_path)
        class_confidence_signature = saved_model.signature_def['confidences']
        class_confidence_signature_tensor_name = class_confidence_signature.outputs['output'].name

        sequence_input_tensor_name = saved_model.signature_def['confidences'].inputs['sequence'].name
        sequence_lengths_input_tensor_name = saved_model.signature_def['confidences'].inputs['sequence_length'].name

        for record in fasta_reader(in_fasta):
            seq = record[1].rstrip('*')
            confidences_by_class = sess.run(
                class_confidence_signature_tensor_name,
                {
                    sequence_input_tensor_name: [residues_to_one_hot(seq)],
                    sequence_lengths_input_tensor_name: [len(seq)],
                }
            )
            predictions.append((record[0], confidences_by_class))            

    with open(pfam_accs_path, encoding='utf-8') as f:
        vocab = json.loads(f.read())
        for prediction in predictions:
            print(f'{prediction[0]}\t{vocab[np.argmax(prediction[1])]}')


# def _test_pad_one_hot():
#     input_one_hot = residues_to_one_hot('ACX')
#     expected = np.array(input_one_hot.tolist() + np.zeros((4, 20)).tolist())
#     actual = pad_one_hot_sequence(input_one_hot, 7)
# 
#     np.testing.assert_allclose(expected, actual)
# 
# def _test_residues_to_one_hot():
#     expected = np.zeros((3, 20))
#     expected[0, 0] = 1.   # Amino acid A
#     expected[1, 1] = 1.   # Amino acid C
#     expected[2, :] = .05  # Amino acid X
# 
#     actual = residues_to_one_hot('ACX')
#     np.testing.assert_allclose(actual, expected)

if __name__ == '__main__':
    main()
